# Lab 4 - Rob Johnson - PCC CS162 - 10/12/2018
CC=g++
CFLAGS= -c -Wall -std=c++11
all: prog
prog: main.o task.o tasklist.o
	$(CC) main.o task.o tasklist.o -o prog
main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp
task.o: task.cpp
	$(CC) $(CFLAGS) task.cpp
tasklist.o: tasklist.cpp
	$(CC) $(CFLAGS) tasklist.cpp
clean:
	rm -rf *.o
