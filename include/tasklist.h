#ifndef TASKLIST_H

#define TASKLIST_H

#include "main.h"
#include "task.h"
#include <fstream>

struct node {
    Task data;
    node* next;
};

class Tasklist
{
    public:
        Tasklist(void);
        ~Tasklist(void);

        void addTask(Task newtask);

        void printList(void);
        void printRange(int year0, int month0, int day0, int year1, int month1, int day1);
        void printIncomplete(void);
        void markTaskComplete(char*);
        void deleteTask(char*);
        void fileWrite(std::ofstream&);

    private:

        void copyTask(Task* destination, Task* source);
        int date_compare(int year, int month, int day, int taskyear, int taskmonth, int taskday);
        bool date_within_range(int year0, int month0, int day0, int year1, int month1, int day1, Task task);
        node* isWordInList(node* current_node, char* word);
        void deleteList(void);
        node* head;
};

#endif // TASKLIST_H
