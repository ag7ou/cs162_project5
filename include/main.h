#ifndef MAIN_H

#define MAIN_H

#define MAX_STRING 250
#define INPUT_FILENAME ("tasks.txt")
#define OUTPUT_FILENAME ("tasks.txt")
#define YEAR_UPPER_LIMIT (2078)
#define YEAR_LOWER_LIMIT (2017)
#define MONTH_UPPER_LIMIT (12)
#define MONTH_LOWER_LIMIT (1)
#define DAY_UPPER_LIMIT (31)
#define DAY_LOWER_LIMIT (1)

#endif // MAIN_H
