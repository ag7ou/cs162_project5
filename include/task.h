#ifndef TASK_H
#define TASK_H

#include "main.h"   // needed for the MAX_STRING macro

class Task
{
    public:
        Task(void);    // default constructor
        char* GetName();
        void SetName(char val[MAX_STRING]);
        char* GetDescription();
        void SetDescription(char val[MAX_STRING]);
        int GetDay();
        void SetDay(int val);
        int GetMonth();
        void SetMonth(int val);
        int GetYear();
        void SetYear(int val);
        int getDate(void);
        bool GetComplete();
        void SetComplete(bool val);
        void print(void);

    protected:

    private:
        char name[MAX_STRING];
        char description[MAX_STRING];
        int day;
        int month;
        int year;
        bool complete;
};

#endif // TASK_H
