// Project 5 - Rob Johnson - pcc cs162 - 12/4/18

#include <iostream>
#include <fstream>
#include "main.h"
#include "task.h"
#include <cstring>

#include "tasklist.h"

using namespace std;

void input_number(int& number, int upper_limit, int lower_limit)
{
    number = 0;

    while(number == 0)
    {
        cin >> number;
        while(cin.fail() || number < lower_limit || number > upper_limit)
        {
            cin.clear();
            cin.ignore(MAX_STRING, '\n');
            cout << "Invalid entry. Should be an integer between " << lower_limit << " and " << upper_limit << endl;
            cin >> number;
        }
    }
}


void input_date(int& year, int& month, int& day)
{
    cout << "Enter year: ";
    input_number(year, YEAR_UPPER_LIMIT, YEAR_LOWER_LIMIT);

    cout << "Enter month: ";
    input_number(month, MONTH_UPPER_LIMIT, MONTH_LOWER_LIMIT);

    cout << "Enter day: ";
    input_number(day, DAY_UPPER_LIMIT, DAY_LOWER_LIMIT);
}


inline void input_task_name(char* name)
{
    cin.ignore(MAX_STRING,'\n');
    cout << "Enter task name: ";
    cin.getline(name, MAX_STRING);
}


void add_task(Tasklist &tl)
{
    Task newtask;
    char name[MAX_STRING], desc[MAX_STRING];
    int year, month, day;

    cin.ignore(MAX_STRING,'\n');

    cout << "Enter task name: ";
    cin.getline(name, MAX_STRING);
    newtask.SetName(name);

    cout << "Enter task description: ";
    cin.getline(desc, MAX_STRING);
    newtask.SetDescription(desc);

    input_date(year, month, day);
    newtask.SetYear(year);
    newtask.SetMonth(month);
    newtask.SetDay(day);

    newtask.SetComplete(false);  // Always setting to false b/c it wouldn't make sense to add a completed item to the list.

    tl.addTask(newtask);

}


// returns the number of entries that have been read
void fileRead(Tasklist &tl)
{
    ifstream infile;

    infile.open(INPUT_FILENAME);
    if(infile)
    {
        infile.peek(); // tests for end of file w/o moving the read pointer ahead
        for(int i = 0; !infile.eof(); ++i)
        {
            Task newtask;
            char name[MAX_STRING];
            infile.getline(name, MAX_STRING);
            newtask.SetName(name);

            char description[MAX_STRING];
            infile.getline(description, MAX_STRING);
            newtask.SetDescription(description);

            int year, month, day;
            infile >> month;
            infile.ignore(MAX_STRING,'\n');

            infile >> day;
            infile.ignore(MAX_STRING,'\n');

            infile >> year;
            infile.ignore(MAX_STRING,'\n');

            newtask.SetYear(year);
            newtask.SetMonth(month);
            newtask.SetDay(day);

            bool complete;
            infile >> complete;
            infile.ignore(MAX_STRING,'\n');
            newtask.SetComplete(complete);

            tl.addTask(newtask);

            infile.peek();
        }
        infile.close();
    }
    else
    {
        cout << "Error opening input file." << endl;
    }
}


void printRange(Tasklist &tl)
{
    int year0, year1, month0, month1, day0, day1;
    cout << "Enter the first date." << endl;
    input_date(year0, month0, day0);
    cout << "Enter the second date." << endl;
    input_date(year1, month1, day1);
    tl.printRange(year0, month0, day0, year1, month1, day1);
}


void markComplete(Tasklist &tl)
{
    char name[MAX_STRING];
    input_task_name(name);
    tl.markTaskComplete(name);
}


void deleteTask(Tasklist &tl)
{
    char name[MAX_STRING];
    input_task_name(name);
    tl.deleteTask(name);
}


void menu(Tasklist &tl)
{
    bool repeat = true;

    do
    {
        char response;

        cout << endl << "********MAIN MENU**********\n1 - Print All Tasks\n2 - Print Tasks in Date Range\n3 - Print incomplete tasks\n4 - Complete Task\n5 - Add Task\n6 - Delete task\n7 - quit\nEnter choice: ";
        cin >> response;

        if(response == '1')
        {
            tl.printList();
        }
        else if(response == '2')
        {
            printRange(tl);
        }
        else if(response == '3')
        {
            tl.printIncomplete();
        }
        else if(response == '4')
        {
            markComplete(tl);
        }
        else if(response == '5')
        {
            add_task(tl);
        }
        else if(response == '6')
        {
            deleteTask(tl);
        }
        else if(response == '7')
        {
            repeat = false;
        }
        else
        {
            cout << "Invalid entry. Please try again with a valid input." << endl;
            if(cin.fail())
            {
                cin.clear();
                cin.ignore(MAX_STRING, '\n');
            }
        }
    } while(repeat == true);
}


int main(void)
{

    Tasklist manager;

    cout << "******* Task Manager App *******" << endl;

    fileRead(manager);     // Load tasks from file
    menu(manager);

    ofstream outfile;

    outfile.open(OUTPUT_FILENAME);
    manager.fileWrite(outfile);    // save tasks to file.
    outfile.close();

    // no need to manually trigger the deletion of the list. Destructor will delete all tasks.

    return 0;
}
