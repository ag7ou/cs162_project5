#include "task.h"
#include <cstring>
#include <iostream>

using namespace std;

Task::Task(void)
{
    strncpy(name, (char*)&"", MAX_STRING);
    strncpy(description, (char*)&"", MAX_STRING);
    day = 0;
    month = 0;
    year = 0;
    complete = false;
}

char* Task::GetName()
{
    return name;
}

void Task::SetName(char val[MAX_STRING])
{
    strncpy(name, val, MAX_STRING);
}

char* Task::GetDescription()
{
    return description;
}

void Task::SetDescription(char val[MAX_STRING])
{
    strncpy(description, val, MAX_STRING);
}

int Task::GetDay()
{
    return day;
}

void Task::SetDay(int val)
{
    if(val >= DAY_LOWER_LIMIT && val <= DAY_UPPER_LIMIT)
    {
        day = val;
    }
}

int Task::GetMonth()
{
    return month;
}

void Task::SetMonth(int val)
{
    if(val >= MONTH_LOWER_LIMIT && val <= MONTH_UPPER_LIMIT)
    {
        month = val;
    }
}

int Task::GetYear()
{
    return year;
}

void Task::SetYear(int val)
{
    if(val >= YEAR_LOWER_LIMIT && val <= YEAR_UPPER_LIMIT)
    {
        year = val;
    }
}

int Task::getDate(void)
{
    return (10000 * year) + (100 * month) + day;
}

bool Task::GetComplete()
{
    return complete;
}

void Task::SetComplete(bool val)
{
    complete = val;
}

void Task::print(void)
{
    cout << "------------------------" << endl;
    cout << "Name: " << name << endl;
    cout << "Description: " << description << endl;
    cout << "Date: " << month << "/" << day << "/" << year << endl;
    if(complete)
    {
        cout << "Task is complete." << endl;
    }
    else
    {
        cout << "Task is NOT complete." << endl;
    }
    cout << "------------------------" << endl;
}
