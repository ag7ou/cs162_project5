#include "tasklist.h"
#include "task.h"
#include "cstring"
#include <iostream>

using namespace std;

Tasklist::Tasklist(void)  // default constructor
{
    head = NULL;
}

Tasklist::~Tasklist(void)  // default destructor
{
    deleteList();
}

void Tasklist::copyTask(Task* destination, Task* source)
{
    destination->SetName(source->GetName());
    destination->SetDescription(source->GetDescription());
    destination->SetDay(source->GetDay());
    destination->SetMonth(source->GetMonth());
    destination->SetYear(source->GetYear());
    destination->SetComplete(source->GetComplete());
}

void Tasklist::addTask(Task newtask)
{
    node *newnode = new node;
    copyTask(&newnode->data, &newtask);
    newnode->next = NULL;

    if(head)
    {
        node* current = head;
        node* previous = NULL;

        while(current)
        {
            if(newnode->data.getDate() > current->data.getDate())
            {
                if(current->next == NULL)   // last position
                {
                    current->next = newnode;
                    newnode->next = NULL;
                    break;
                }
                else    // increment to next
                {
                    previous = current;
                    current = current->next;
                }
            }
            else    // newnode->data <= current->next
            {
                if(previous == NULL) // first position
                {
                    head = newnode;
                    newnode->next = current;
                }
                else // falls between 2 records
                {
                    previous->next = newnode;
                    newnode->next = current;
                }
                break;
            }
        }
    }
    else    // fist node of list
    {
        head = newnode;
        head->next = NULL;
    }
}

void Tasklist::printList(void)
{
    node *current_node = head;

    while(current_node != NULL)
    {
        current_node->data.print();
        current_node = current_node->next;
    }
}

void Tasklist::printRange(int year0, int month0, int day0, int year1, int month1, int day1)
{
    node *current_node = head;

    while(current_node != NULL)
    {
        if(date_within_range(year0, month0, day0, year1, month1, day1, current_node->data) == true) {
            current_node->data.print();
        }
        current_node = current_node->next;
    }
}

void Tasklist::printIncomplete(void)
{
    node *current_node = head;

    while(current_node != NULL)
    {
        if(current_node->data.GetComplete() == false) {
            current_node->data.print();
        }
        current_node = current_node->next;
    }
}

void Tasklist::markTaskComplete(char* taskname)
{
    node* target = isWordInList(head, taskname);
    if(target != NULL)
    {
        target->data.SetComplete(true);
        cout << "Task set as completed." << endl;
    }
    else
    {
        cout << "Task NOT set as completed." << endl;
    }
}

void Tasklist::deleteTask(char* taskname)
{
    node *current_node = head;
    node* previous = NULL;

    while(current_node != NULL)
    {
        if(strcmp(current_node->data.GetName(), taskname) == 0)
        {
            cout << "Task found." << endl;
            if(previous == NULL) {
                head = current_node->next;
            } else {
                previous->next = current_node->next;
            }
            delete current_node;
            return;
        }
        previous = current_node;
        current_node = current_node->next;
    }
    cout << "Task NOT found." << endl;
}

void Tasklist::fileWrite(ofstream& outputFile)
{
    node *current_node = head;

    while(current_node != NULL)
    {
        outputFile << current_node->data.GetName() << endl;
        outputFile << current_node->data.GetDescription() << endl;
        outputFile << current_node->data.GetMonth() << endl;
        outputFile << current_node->data.GetDay() << endl;
        outputFile << current_node->data.GetYear() << endl;
        outputFile << current_node->data.GetComplete() << endl;

        current_node = current_node->next;
    }
}

node* Tasklist::isWordInList(node* current_node, char* word)
{
    if(current_node == NULL)
    {
        cout << "Task NOT found." << endl;
        return NULL;
    }

    if(strcmp(current_node->data.GetName(), word) == 0)
    {
        cout << "Task found." << endl;
        return current_node;
    }

    return isWordInList(current_node->next, word);
}

// date comparison function
// inputs: year, month, day, task
// returns -1 if the date in the task is earlier than the supplied date
// returns 0 if the date in the task is the same as the supplied date
// returns 1 if the date in the task is late than the supplied date
int Tasklist::date_compare(int year, int month, int day, int taskyear, int taskmonth, int taskday)
{
    int result;

    if(taskyear < year)
    {
        result = -1;
    }
    else if(taskyear > year)
    {
        result = 1;
    }
    else // taskyear == year
    {
        if(taskmonth < month)
        {
            result = -1;
        }
        else if(taskmonth > month)
        {
            result = 1;
        }
        else // taskmonth == month
        {
            if(taskday < day)
            {
                result = -1;
            }
            else if(taskday > day)
            {
                result = 1;
            }
            else // taskday == day
            {
                result = 0;
            }
        }
    }

    return result;
}


// tests to determine whether a task falls within a provided date range
bool Tasklist::date_within_range(int year0, int month0, int day0, int year1, int month1, int day1, Task task)
{
    // by adding the result of date_compare for each of the dates and taking the absolute value of the result
    // it is possible to determine whether the date is within the range, regardless of the order in which the date range is provided
    // if result == 0 then the task date is within the range (or both dates are the same and task date is also the same)
    // if result == 1 then the task date falls on one of the dates
    // if result == 2 then the task is earlier or later than both of the dates and is not valid
    bool result;
    int sum = date_compare(year0, month0, day0, task.GetYear(), task.GetMonth(), task.GetDay())
            + date_compare(year1, month1, day1, task.GetYear(), task.GetMonth(), task.GetDay());

    if(sum > -2 && sum < 2)
    {
        result = true;
    }
    else
    {
        result = false;
    }

    return result;
}

void Tasklist::deleteList(void)
{
    while(head != NULL)
    {
        node* next = head->next;
        delete head;
        head = next;
    }
}
